# Email confirmation

Send confirmation by email to the user after the form has been submitted.

You can include an option (a checkbox) in your form that will send an email. This email with contain the `Thankyou` message.

Create a `Text Field` with the `type` set as `email` as shown below.

![Enable email confirmation](/images/email_confirmation.en.png)

After you have saved the form, you will have a new option `Confirmation`, that can be enabled or disabled.

When enabled, the user will be able to choose if they wish to recieve confirmation by email.

> If by chance you define two or more Text Fields with the type `email`,
LiberaForms will use the first field as the recipient address.





