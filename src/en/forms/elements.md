# Form elements

A form contains `elements`. Elements are the building blocks of a form.

There are two basic types of elements:

1. `Fields`: Fields solicit information from the user.
2. `Texts`: Texts only display text. They can be used to orientate the user. 

## 1. Fields

There are different types of fields. Short texts, long texts, checkboxes, dates, etc.

Fields share these common options:

* `Required`: The form will not submit if this element is not complete.
* `Label`: A short text. This is the new element's name.
* `Help text`: A longer text. Use this to a description.


### Text Field

Displays a Text field. Text fields are short, one line texts.  

<label for="name">Name</label><br>
<input id="name" type="text"></input>

Use this element to solicit information like a Name, Postcode, etc.


### Text Area

Displays a Textarea field.

<label for="opinion">Your opinion</label><br>
<textarea id="opinion" rows="4" cols="50"></textarea>

Use this element to solicit extended information like an Opinion.


### Select

Displays a dropdown list of selectable options.

<select>
 <option>Early morning session</option>
 <option>Early afternoon session</option>
 <option>Evening session</option>
</select>

* `Options`: Define selectable options. See [Multiple options](#multiple-options).


### Checkbox Group

Displays a group of checkboxes. Multiple options may be selected.

<input type="checkbox" name="chk_group" />&nbsp; Early morning session <br />
<input type="checkbox" name="chk_group" />&nbsp; Early afternoon session <br />
<input type="checkbox" name="chk_group" />&nbsp; Evening session <br />

* `Inline`: Display the options horizontally.
* `Options`: Define selectable options. See [Multiple options](#multiple-options).


### Radio Group

Displays a group of radios. Only one option may be selected.

<input type="radio" id="morning" name="animal">&nbsp;
<label for="morning">Early morning session</label><br>
<input type="radio" id="afternoon" name="animal">&nbsp;
<label for="afternoon">Early afternoon session</label><br>
<input type="radio" id="evening" name="animal">&nbsp;
<label for="evening">Evening session</label> 

* `Inline`: Display the options horizontally.
* `Options`: Define selectable options. See [Multiple options](#multiple-options).


### Date Field

Displays a Date field.

<input style="height:2em" type="date" id="birthday" name="birthday" />


### Number

Displays a Number field.

<input type="number" min="1" max="5">

---


## Multiple options



The left column contains the text the user will see.
The right column contains the value that will be saved in the database.

![Select element options](/images/select_element_options.en.png)

For example, you may wish to display an option called 'Early afternoon session'. That is clear for the user of your form, but you only really need to save the word 'afternoon' in the database.

> Using short names for the database will make it easier later when reading spreadsheets for example.

---

## 2. Texts

These elements only display text. They do not solicit information.

### Header

Displays a line of text to the user.

* `Label`: The text to be displayed.
* `Type`: The size of the text. `h1` biggest, `h6` smallest.


### Paragraph

Displays a paragraph of text to the user.

* `Content`: The text to be displayed.


