# mdBook

Install curl

```bash
apt-get install curl
```

Add a user to serve mdBook

```bash
adduser --disabled-password mdbook
```

Open a session as mdbook user


```bash
su - mdbook
```

## Install Rust
```bash
curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
```

## Install mdBook with localization support

Note: 2021/04/05

> mdbook doesn't support localisation yet, so we are using code that is
still work on progress to create our documentation with localisation.
Note that this code still has some rough edges.
See github.com/rust-lang/mdBook/pull/1306 for more details.

Until those changes have been merged, we will pull the code from the git repository

```bash
cargo install mdbook --git https://github.com/Ruin0x11/mdBook.git --branch localization --rev d06249b
```


```bash
mdbook init
mdbook init <your_book_name>
```

```bash
cd <your_book_name>
```

## Create languages

Edit `book.toml`

```
[book]
authors = []
src = "src"

[language.en]
name = "English"
title = "Documentation"

[language.es]
name = "Español"
title = "Documentación"

[language.ca]
name = "Català"
title = "Documentació"
```

Create the language directories

```bash
cp -a en es
cp -a en ca
```

# Compile the book

mdBook compiles the md files in `./src` creating the static site in `./book`
There are many ways to do this add directories and md file to your `./src`.
You could for example edit the `./src` files and directories in your PC,
compile them and then upload the `./book` files to your webserver.

But we will edit the `./src` in situ (you could also use git for example).
So, we need mdBook to compile the site when there are changes.

```bash
mdbook watch &
```


## Nginx

We use nginx to server the static files.

```
server {
    listen         80;
    server_name    example.com;
    return         301 https://$server_name$request_uri;
}
server {
    listen 443 ssl;
    server_name example.com;

    ssl_trusted_certificate   /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate           /etc/letsencrypt/live/example.com/fullchain.pem;
    ssl_certificate_key       /etc/letsencrypt/live/example.com/privkey.pem;

	location / {
		root   /var/www/html/mdbook/en;
        }
	location /en {
		alias   /var/www/html/mdbook/en;
        }
	location /es {
		alias   /var/www/html/mdbook/es;
        }
	location /ca {
		alias   /var/www/html/mdbook/ca;
        }
    
    access_log /var/log/nginx/example.com.access.log;
    error_log /var/log/nginx/example.com.error.log notice;
}
```
